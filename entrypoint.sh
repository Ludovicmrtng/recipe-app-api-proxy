#!/bin/sh

#####################################################################
# Run our application inside Docker                                 #
# Populate the default.conf.tpl with env variables                  #
# Start nginx service                                               #
#####################################################################

# During the execution of script if any lines failed then return a error 
set -e 

##############################################################################################################################
#Pass in the env variable to these values in our template                                                                    #
# < to pass in the template fil that we want to substitute the env for                                                       #
# > Ouput the updated config, take in the template populate the env variable and output to new file                          #
##############################################################################################################################

envsubst < /etc/nginx/default.conf.tpl > /etc/nginx/conf.d/default.conf

# Run NGINX in the foreground - Note: It is not recommended to nginx in backaground docker!!
nginx -g 'daemon off;'